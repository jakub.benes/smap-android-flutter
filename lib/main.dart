import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:rxdart/subjects.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();

// Streams are created so that app can respond to notification-related events since the plugin is initialised in the `main` function
final BehaviorSubject<ReceivedNotification> didReceiveLocalNotificationSubject =
BehaviorSubject<ReceivedNotification>();

final BehaviorSubject<String> selectNotificationSubject =
BehaviorSubject<String>();

List<LastBoxNotification> LastBoxNotifications = new List<LastBoxNotification>();

class ReceivedNotification {
  final int id;
  final String title;
  final String body;
  final String payload;

  ReceivedNotification(
      {@required this.id,
        @required this.title,
        @required this.body,
        @required this.payload});
}

class LastBoxNotification {
  final int id;
  final int idColdBox;
  final DateTime date;

  LastBoxNotification(
      {@required this.id,
        @required this.idColdBox,
        @required this.date});
}

Future<void> main() async {
  // needed if you intend to initialize in the `main` function
  WidgetsFlutterBinding.ensureInitialized();
  // NOTE: if you want to find out if the app was launched via notification then you could use the following call and then do something like
  // change the default route of the app
  // var notificationAppLaunchDetails =
  //     await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

  var initializationSettingsAndroid = AndroidInitializationSettings('app_icon');
  var initializationSettingsIOS = IOSInitializationSettings(
      onDidReceiveLocalNotification:
          (int id, String title, String body, String payload) async {
        didReceiveLocalNotificationSubject.add(ReceivedNotification(
            id: id, title: title, body: body, payload: payload));
      });
  var initializationSettings = InitializationSettings(
      initializationSettingsAndroid, initializationSettingsIOS);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings,
      onSelectNotification: (String payload) async {
        if (payload != null) {
          debugPrint('notification payload: ' + payload);
        }
        selectNotificationSubject.add(payload);
      });
  runApp(
      MyApp()
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Chytrý chladící box',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Chytré chladící boxy'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

Future<List<ColdBox>> fetchColdBoxes(http.Client client) async {
  final response = await http.get('http://smap.logisoft.cz/api/coldboxes');
  List<ColdBox> boxes = parseColdBoxes(response.body);
  Future<List<ColdBox>> fBoxes = compute(parseColdBoxes, response.body);

  for (var box in boxes) {
    if(box.isError){
      var androidPlatformChannelSpecifics = AndroidNotificationDetails(
          'your channel id', 'your channel name', 'your channel description',
          importance: Importance.Max, priority: Priority.High, ticker: 'ticker');
      var iOSPlatformChannelSpecifics = IOSNotificationDetails();
      var platformChannelSpecifics = NotificationDetails(
          androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
      await flutterLocalNotificationsPlugin.show(
          0, 'Varování', 'Problém v chladícím boxu: ' + box.name, platformChannelSpecifics,
          payload: 'item x');
    }
  }


  return fBoxes;
}

// A function that converts a response body into a List
List<ColdBox> parseColdBoxes(String responseBody) {
  final parsed = jsonDecode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<ColdBox>((json) => ColdBox.fromJson(json)).toList();
}

class ColdBox {
  final int id;
  final String name;
  final double temperature;
  final double minTemperature;
  final double maxTemperature;
  final bool isError;

  ColdBox({this.id, this.name, this.temperature, this.minTemperature, this.maxTemperature, this.isError});

  factory ColdBox.fromJson(Map<String, dynamic> json) {
    ColdBox c = ColdBox(
        id: json['id'],
        name: json['name'],
        temperature: json['temperature'] != null ? json['temperature'].toDouble() : null,
        minTemperature: json['minTemperature'].toDouble(),
        maxTemperature: json['maxTemperature'].toDouble(),
        isError: json['isError']);
    if(c.isError){

    }
    return c;
  }
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    const oneSecond = const Duration(seconds: 5);
    new Timer.periodic(oneSecond, (Timer t) => setState(() {}));

    didReceiveLocalNotificationSubject.stream
        .listen((ReceivedNotification receivedNotification) async {
      await showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          title: receivedNotification.title != null
              ? Text(receivedNotification.title)
              : null,
          content: receivedNotification.body != null
              ? Text(receivedNotification.body)
              : null,
          actions: [
            CupertinoDialogAction(
              isDefaultAction: true,
              child: Text('Ok'),
              onPressed: () async {
                Navigator.of(context, rootNavigator: true).pop();
                await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) =>
                        SecondScreen(receivedNotification.payload),
                  ),
                );
              },
            )
          ],
        ),
      );
    });
    selectNotificationSubject.stream.listen((String payload) async {
      await Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => SecondScreen(payload)),
      );
    });
  }

  @override
  void dispose() {
    didReceiveLocalNotificationSubject.close();
    selectNotificationSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
        appBar: AppBar(
          // Here we take the value from the MyHomePage object that was created by
          // the App.build method, and use it to set our appbar title.
          title: Text(widget.title),
        ),
        body: Center(
            child: FutureBuilder<List<ColdBox>>(
          future: fetchColdBoxes(http.Client()),
          builder: (context, snapshot) {
            if (snapshot.hasError) print(snapshot.error);

            return snapshot.hasData
                ? ColdBoxesList(coldBoxes: snapshot.data)
                : Center(child: CircularProgressIndicator());
          },
        )));
  }
}

class ColdBoxesList extends StatelessWidget {
  final List<ColdBox> coldBoxes;

  ColdBoxesList({Key key, this.coldBoxes}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 1,
      ),
      itemCount: coldBoxes.length,
      itemBuilder: (context, index) {
        return Container(
          margin: const EdgeInsets.all(5.0),
          padding: const EdgeInsets.all(12.0),
          height: 300,
          decoration: BoxDecoration(
              border: Border.all(
            color: Colors.lightBlueAccent,
            width: 2,
          )),
          child: Column(
            children: [
              Center(
                  child: Text(
                coldBoxes[index].name,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              )),
              Container(
                  margin: const EdgeInsets.only(top: 100),
                  child: Center(
                    child: Text(
                      (coldBoxes[index].isError && coldBoxes[index].temperature == null ? "?" : coldBoxes[index].temperature.toString()) + " °C",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 70,
                              color: coldBoxes[index].isError ? Colors.red : Colors.black
                          ),
                    ),
                  )),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(children: [
                    Container(
                      margin: const EdgeInsets.only(top: 100),
                      child: Text(
                        "min: " +
                            coldBoxes[index].minTemperature.toString() +
                            " °C max: " +
                            coldBoxes[index].maxTemperature.toString() +
                            " °C",
                        style: TextStyle(fontSize: 10),
                      ),
                    )
                  ]),
                  /*
                  Column(children: [
                    Container(
                      margin: const EdgeInsets.only(top: 40),
                      child: Column(
                        children: [
                          IconButton(
                            icon: Icon(Icons.insert_chart),
                            iconSize: 40,
                            color: Colors.blue,
                            onPressed: () {
                            },
                          ),
                          Text("Historie")
                        ],
                      )
                    )
                  ])
                  */
                ],
              )
            ],
          ),
        );
      },
    );
  }
}

class SecondScreen extends StatefulWidget {
  SecondScreen(this.payload);

  final String payload;

  @override
  State<StatefulWidget> createState() => SecondScreenState();
}

class SecondScreenState extends State<SecondScreen> {
  String _payload;
  @override
  void initState() {
    super.initState();
    _payload = widget.payload;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Second Screen with payload: ${(_payload ?? '')}'),
      ),
      body: Center(
        child: RaisedButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: Text('Go back!'),
        ),
      ),
    );
  }
}
